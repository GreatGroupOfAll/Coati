const nedb = require('nedb')
const browserInfo = new nedb('./database/browserData.db')
const cookies = new nedb('./database/cookies.db')
browserInfo.loadDatabase()
cookies.loadDatabase()


module.exports = {
    browserDB : browserInfo,
    cookieDB : cookies
}