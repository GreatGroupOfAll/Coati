const EventEmitter = require('events').EventEmitter
const messageBus = new EventEmitter()
messageBus.setMaxListeners(100)

var ids = []

module.exports = {
  ids: ids,
  messageBus: messageBus,
}