const express = require('express')
const path = require('path');
const app = express()
const bodyParser = require('body-parser')

app.set('view engine', 'ejs')
app.use('/static', express.static(path.join(__dirname, 'public')))
app.use(bodyParser.text());

app.use('/', require('./routes/pages'))

app.use('/client', require('./routes/client'))

app.use('/api', require('./routes/api'))

app.listen(3050, () => console.log('Server liston on port 3050'))

