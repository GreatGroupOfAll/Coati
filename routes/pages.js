const express = require('express')
const router = express.Router()
const values = require('../models/values')

router.get('/', function (req, res) {
  res.render('pages/index')
})

router.get('/list', function (req, res) {
  res.render('pages/list', { ids: values.ids })
})

module.exports = router