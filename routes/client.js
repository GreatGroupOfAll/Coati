const express = require('express')
const router = express.Router()
const values = require('../models/values')

router.get('/messages/:uid', function (req, res) {
  let responded = false
  let id = req.params.uid
  if (values.ids.indexOf(id) == -1) {
    values.ids.push(id)
  }
  let test = function (data) {
    console.log('test ' + data)
    res.send(data)
    responded = true
    values.ids = values.ids.filter(e => e !== id)
  }
  let addMessageListener = function (res) {
    values.messageBus.once('message-' + id, test)
  }
  addMessageListener(res)
  setTimeout(() => {
    if (!responded) {
      console.log('removed: '  + id + " " + test)
      values.ids = values.ids.filter(e => e !== id)
      values.messageBus.removeListener("message-" + id, test);
      res.status(204).end();
    }
  }, 10000);
  console.log('List of ids: ' + values.ids)
})

module.exports = router